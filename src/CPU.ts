import { Opcode, StatusFlags, U8, U16 } from "./Types";

/**
 * @param memory Eventually won't be an array of numbers.
 */
export function Make6502(memory: number[]) {
    let A = 0;
    let X = 0;
    let Y = 0;
    let SP = 0;
    let PC = 0;
    let STATUS = 0;
    
    return {
        getRegisters() {
            return { A, X, Y, SP, PC, STATUS }
        },
        dispatch,
        setProgramCounterAddress(location: number) {
            PC = location & U16.Max;
        },
    }

    function dispatch(): void {
        const instruction = read_u8() as Opcode;
        switch (instruction) {
            // Add with Carry
            case Opcode.ADC_Imm:
                return ADC(A, read_u8());
            case Opcode.ADC_ZP:
                return ADC(A, readZeroPage());
            case Opcode.ADC_ZP_X:
                return ADC(A, readZeroPageX());
            case Opcode.ADC_Abs:
                return ADC(A, readAbsolute());
            case Opcode.ADC_Abs_X:
                return ADC(A, readAbsoluteX());
            case Opcode.ADC_Abs_Y:
                return ADC(A, readAbsoluteX());
            case Opcode.ADC_Ind_X:
                return ADC(A, readIndirectX());
            case Opcode.ADC_Ind_Y:
                return ADC(A, readIndirectY());

            // AND (bitwise on accumulator)
            case Opcode.AND_Imm:
                return AND(A, read_u8());
            case Opcode.AND_ZP:
                return AND(A, readZeroPage());
            case Opcode.AND_ZP_X:
                return AND(A, readZeroPageX());
            case Opcode.AND_Abs:
                return AND(A, readAbsolute());
            case Opcode.AND_Abs_X:
                return AND(A, readAbsoluteX());
            case Opcode.AND_Abs_Y:
                return AND(A, readAbsoluteY());
            case Opcode.AND_Ind_X:
                return AND(A, readIndirectX());
            case Opcode.AND_Ind_Y:
                return AND(A, readIndirectY());

            // ASL (Arithmetic Shift Left)
            case Opcode.ASL_Acc:
                A = ASL(A);
                return;
            case Opcode.ASL_ZP:
                return PerformOnMemory(ASL, readZeroPageAddress);
            case Opcode.ASL_ZP_X:
                return PerformOnMemory(ASL, readZeroPageXAddress);
            case Opcode.ASL_ABS:
                return PerformOnMemory(ASL, readAbsoluteAddress);
            case Opcode.ASL_ABS_X:
                return PerformOnMemory(ASL, readAbsoluteXAddress);

            case Opcode.BIT_ZP: {
                const result = A & readZeroPage();
                setNegativeAndZero(result);
                setAppropriateStatus((result & U8.BIT_6) !== 0, StatusFlags.Overflow);
                return;
            }
            case Opcode.BIT_ABS: {
                const result = A & readAbsolute();
                setNegativeAndZero(result);
                setAppropriateStatus((result & U8.BIT_6) !== 0, StatusFlags.Overflow);
                return;
            }

            // Branch if...
            // PLus
            case Opcode.BPL:
                return BranchOn(~STATUS, StatusFlags.Negative);
            // MInus
            case Opcode.BMI:
                return BranchOn(STATUS, StatusFlags.Negative);
            // oVerflow Clear
            case Opcode.BVC:
                return BranchOn(~STATUS, StatusFlags.Overflow);
            // oVerflow Set
            case Opcode.BVS:
                return BranchOn(STATUS, StatusFlags.Overflow);
            // Carry Clear
            case Opcode.BCC:
                return BranchOn(~STATUS, StatusFlags.Carry);
            case Opcode.BCS:
                return BranchOn(STATUS, StatusFlags.Carry);
            // Not Equal
            case Opcode.BNE:
                return BranchOn(~STATUS, StatusFlags.Zero);
            // EQual
            case Opcode.BEQ:
                return BranchOn(STATUS, StatusFlags.Zero);

            case Opcode.BRK:
                return BRK();

            case Opcode.CMP_Imm:
                return CMP(A, read_u8());
            case Opcode.CMP_ZP:
                return CMP(A, readZeroPage());
            case Opcode.CMP_ZP_X:
                return CMP(A, readZeroPageX());
            case Opcode.CMP_Abs:
                return CMP(A, readAbsolute());
            case Opcode.CMP_Abs_X:
                return CMP(A, readAbsoluteX());
            case Opcode.CMP_Abs_Y:
                return CMP(A, readAbsoluteY());
            case Opcode.CMP_Ind_X:
                return CMP(A, readIndirectX());
            case Opcode.CMP_Ind_Y:
                return CMP(A, readIndirectY());

            case Opcode.CPX_Imm:
                return CMP(X, read_u8());
            case Opcode.CPX_ZP:
                return CMP(X, readZeroPage());
            case Opcode.CPX_Abs:
                return CMP(X, readAbsolute());

            case Opcode.CPY_Imm:
                return CMP(Y, read_u8());
            case Opcode.CPY_ZP:
                return CMP(Y, readZeroPage());
            case Opcode.CPY_Abs:
                return CMP(Y, readAbsolute());

            case Opcode.DEC_ZP:
                return DEC(readZeroPageAddress);
            case Opcode.DEC_ZP_X:
                return DEC(readZeroPageXAddress);
            case Opcode.DEC_Abs:
                return DEC(readAbsoluteAddress);
            case Opcode.DEC_Abs_X:
                return DEC(readZeroPageXAddress);

            // XOR
            case Opcode.EOR_Imm:
                return EOR(read_u8());
            case Opcode.EOR_ZP:
                return EOR(readZeroPage());
            case Opcode.EOR_ZP_X:
                return EOR(readZeroPageX());
            case Opcode.EOR_Abs:
                return EOR(readAbsolute());
            case Opcode.EOR_Abs_X:
                return EOR(readAbsoluteX());
            case Opcode.EOR_Abs_Y:
                return EOR(readAbsoluteY());
            case Opcode.EOR_Ind_X:
                return EOR(readIndirectX());
            case Opcode.EOR_Ind_Y:
                return EOR(readIndirectY());

            // Flag instructions
            // CLear Carry
            case Opcode.CLC:
                STATUS &= ~StatusFlags.Carry;
                return;
            // SEt Carry
            case Opcode.SEC:
                STATUS |= StatusFlags.Carry;
                return;
            // CLear Interrupt
            case Opcode.CLI:
                STATUS &= ~StatusFlags.InterruptDisable;
                return;
            // SEt Interrupt
            case Opcode.SEI:
                STATUS |= StatusFlags.InterruptDisable;
                return;
            // CLear oVerflow
            case Opcode.CLV:
                STATUS &= ~StatusFlags.Overflow;
                return;
            // CLear Decimal
            case Opcode.CLD:
                STATUS &= ~StatusFlags.InterruptDisable;
                return;
            // SEt Decimal
            case Opcode.SED:
                STATUS |= StatusFlags.InterruptDisable;
                return;

            case Opcode.INC_ZP:
                return INC(readZeroPageAddress);
            case Opcode.INC_ZP_X:
                return INC(readZeroPageXAddress);
            case Opcode.INC_Abs:
                return INC(readAbsoluteAddress);
            case Opcode.INC_Abs_X:
                return INC(readZeroPageXAddress);

            case Opcode.JMP_Abs:
                PC = readAbsoluteAddress();
                return;
            case Opcode.JMP_Ind:
                PC = readIndirectAddress();
                return;

            // Jump to SubRoutine
            case Opcode.JSR:
                return JSR();

            // LoaD Accumulator
            case Opcode.LDA_Imm:
                return setA(read_u8());
            case Opcode.LDA_ZP:
                return setA(readZeroPage());
            case Opcode.LDA_ZP_X:
                return setA(readZeroPageX());
            case Opcode.LDA_Abs:
                return setA(readAbsolute());
            case Opcode.LDA_Abs_X:
                return setA(readAbsoluteX());
            case Opcode.LDA_Abs_Y:
                return setA(readAbsoluteY());
            case Opcode.LDA_Ind_X:
                return setA(readIndirectX());
            case Opcode.LDA_Ind_Y:
                return setA(readIndirectY());

            // LoaD X Register
            case Opcode.LDX_Imm:
                return setX(read_u8());
            case Opcode.LDX_ZP:
                return setX(readZeroPage());
            case Opcode.LDX_ZP_Y:
                return setX(readZeroPageY());
            case Opcode.LDX_Abs:
                return setX(readAbsolute());
            case Opcode.LDX_Abs_Y:
                return setX(readAbsoluteY());

            // LoaD Y Register
            case Opcode.LDY_Imm:
                return setY(read_u8());
            case Opcode.LDY_ZP:
                return setY(readZeroPage());
            case Opcode.LDY_ZP_X:
                return setY(readZeroPageX());
            case Opcode.LDY_Abs:
                return setY(readAbsolute());
            case Opcode.LDY_Abs_X:
                return setY(readAbsoluteX());

            // Logical Shift Right
            case Opcode.LSR_Acc:
                A = LSR(A);
                return;
            case Opcode.LSR_ZP:
                return PerformOnMemory(ASL, readZeroPageAddress);
            case Opcode.LSR_ZP_X:
                return PerformOnMemory(ASL, readZeroPageXAddress);
            case Opcode.LSR_Abs:
                return PerformOnMemory(ASL, readAbsoluteAddress);
            case Opcode.LSR_Abs_X:
                return PerformOnMemory(ASL, readAbsoluteXAddress);

            case Opcode.NOP:
                return;

            // OR w/ Accumulator
            case Opcode.ORA_Imm:
                return ORA(read_u8());
            case Opcode.ORA_ZP:
                return ORA(readZeroPage());
            case Opcode.ORA_ZP_X:
                return ORA(readZeroPageX());
            case Opcode.ORA_Abs:
                return ORA(readAbsolute());
            case Opcode.ORA_Abs_X:
                return ORA(readAbsoluteX());
            case Opcode.ORA_Abs_Y:
                return ORA(readAbsoluteY());
            case Opcode.ORA_Ind_X:
                return ORA(readIndirectX());
            case Opcode.ORA_Ind_Y:
                return ORA(readIndirectY());

            // ---------------------
            // Register Instructions
            // ---------------------

            // X Instructions
            // --------------
            // A -> X
            case Opcode.TAX:
                return setX(A);
            // X -> A
            case Opcode.TXA:
                return setA(X);
            case Opcode.DEX:
                return setX((X - 1) & U8.Max);
            case Opcode.INX:
                return setX((X + 1) & U8.Max);

            // Y Instructions
            // --------------
            case Opcode.TAY:
                return setY(A);
            case Opcode.TYA:
                return setA(Y);
            case Opcode.DEY:
                return setY((Y - 1) & U8.Max);
            case Opcode.INY:
                return setY((Y + 1) & U8.Max);

            // ROtate Left
            case Opcode.ROL_Acc:
                A = ROL(A);
                return;
            case Opcode.ROL_ZP:
                return PerformOnMemory(ROL, readZeroPageAddress);
            case Opcode.ROL_ZP_X:
                return PerformOnMemory(ROL, readZeroPageXAddress);
            case Opcode.ROL_Abs:
                return PerformOnMemory(ROL, readAbsoluteAddress);
            case Opcode.ROL_Abs_X:
                return PerformOnMemory(ROL, readAbsoluteXAddress);

            // ROtate Right
            case Opcode.ROR_Acc:
                A = ROR(A);
                return;
            case Opcode.ROR_ZP:
                return PerformOnMemory(ROR, readZeroPageAddress);
            case Opcode.ROR_ZP_X:
                return PerformOnMemory(ROR, readZeroPageXAddress);
            case Opcode.ROR_Abs:
                return PerformOnMemory(ROR, readAbsoluteAddress);
            case Opcode.ROR_Abs_X:
                return PerformOnMemory(ROR, readAbsoluteXAddress);

            case Opcode.RTI:
                throw new Error("Not implemented");

            // ReTurn from Subroutine
            case Opcode.RTS:
                return RTS();

            // SuBtract w/ Carry
            case Opcode.SBC_Imm:
            case Opcode.SBC_ZP:
            case Opcode.SBC_ZP_X:
            case Opcode.SBC_Abs:
            case Opcode.SBC_Abs_X:
            case Opcode.SBC_Abs_Y:
            case Opcode.SBC_Ind_X:
            case Opcode.SBC_Ind_Y:

            // STore Accumulator
            case Opcode.STA_ZP:
                memory[readZeroPageAddress()] = A;
                return;
            case Opcode.STA_ZP_X:
                memory[readZeroPageXAddress()] = A;
                return;
            case Opcode.STA_Abs:
                memory[readAbsoluteAddress()] = A;
                return;
            case Opcode.STA_Abs_X:
                memory[readAbsoluteXAddress()] = A;
                return;
            case Opcode.STA_Abs_Y:
                memory[readAbsoluteYAddress()] = A;
                return;
            case Opcode.STA_Ind_X:
                memory[readIndirectXAddress()] = A;
                return;
            case Opcode.STA_Ind_Y:
                memory[readIndirectYAddress()] = A;
                return;

            // Transfer X to Stack Ptr
            case Opcode.TXS:
                SP = X;
                return;
            // Transfer Stack Ptr to X
            case Opcode.TSX:
                return setX(SP);

            // Push Accumulator
            case Opcode.PHA:
                memory[pushSP()] = A;
                return;
            // Pull Accumulator
            case Opcode.PLA:
                A = memory[popSP()];
                return;

            // Push processor status
            case Opcode.PHP:
                memory[pushSP()] = STATUS;
                return;
            // Pull processor status
            case Opcode.PLP:
                STATUS = memory[popSP()];
                return;

            case Opcode.STX_ZP:
                memory[readZeroPage()] = X;
                return;
            case Opcode.STX_ZP_Y:
                memory[readZeroPageY()] = X;
                return;
            case Opcode.STX_Abs:
                memory[readAbsolute()] = X;
                return;
            case Opcode.STY_ZP:
                memory[readZeroPage()] = Y;
                return;
            case Opcode.STY_ZP_X:
                memory[readZeroPageX()] = Y;
                return;
            case Opcode.STY_Abs:
                memory[readAbsolute()] = Y;
                return;
            default:
                assertNever(instruction, "Unknown opcode!");
        }
    }

    function ADC(a: number, b: number): void {
        let result = a + b;
        if (STATUS & StatusFlags.Carry) {
            result++;
        }
        const overflow = result >= U8.Max;
        if (overflow) {
            result &= U8.Max;
        }
        setAppropriateStatus(overflow, StatusFlags.Carry);
        A = result;
        setNegativeAndZero(A)
    }

    function AND(a: number, b: number): void {
        A = a & b;
        setNegativeAndZero(A);
    }

    function ASL(arg: number) {
        const oldBit7Set = (arg & U8.BIT_7) !== 0
        const result = (arg << 1) & U8.Max;
        setAppropriateStatus(oldBit7Set, StatusFlags.Carry);
        setNegativeAndZero(result);
        return result;
    }

    function BranchOn(status: number, flag: number) {
        const offset = read_u8();
        if (status & flag) {
            PC = (PC + offset) & U16.Max;
        }
        return;
    }

    function BRK() {
        // Ignore next value.
        read_u8();
        memory[pushSP()] = PC & U16.UpperHalf;
        memory[pushSP()] = PC & U16.LowerHalf;
        STATUS |= StatusFlags.Break;
        memory[pushSP()] = STATUS;
        PC = (memory[0xFFFF] << 8) | (memory[0xFFFE]);
    }

    function CMP(reg: number, operand: number) {
        const result = (reg - operand) & U8.Max;

        setNegativeAndZero(result);
        setAppropriateStatus(reg >= operand, StatusFlags.Carry);
    }

    function DEC(getAddr: () => number) {
        const addr = getAddr();
        const result = (memory[addr] - 1) & U8.Max;
        setNegativeAndZero(result);
        memory[addr] = result;
        return;
    }

    function EOR(operand: number) {
        A = A ^ operand;
        setNegativeAndZero(A);
    }

    function INC(getAddr: () => number) {
        const addr = getAddr();
        const result = (memory[addr] + 1) & U8.Max;
        setNegativeAndZero(result);
        memory[addr] = result;
        return;
    }

    function JSR() {
        const upper = PC & U16.UpperHalf;
        const lower = PC & U16.LowerHalf;
        memory[pushSP()] = upper;
        memory[pushSP()] = lower;
        PC = readAbsoluteAddress();
    }

    function LSR(arg: number) {
        const oldBit0Set = (arg & U8.BIT_0) !== 0;
        const result = arg >> 1;
        setAppropriateStatus(oldBit0Set, StatusFlags.Carry);
        setNegativeAndZero(result);
        return result;
    }

    function ORA(operand: number) {
        A = A | operand;
        setNegativeAndZero(A);
    }

    function ROL(arg: number) {
        const newBit0 = (STATUS & StatusFlags.Carry) ? 1 : 0;
        const oldBit7Set = (arg & U8.BIT_7) !== 0;
        const result = ((arg << 1) & U8.Max) | newBit0;
        setAppropriateStatus(oldBit7Set, StatusFlags.Carry);
        setNegativeAndZero(arg);
        return arg;
    }

    function ROR(arg: number) {
        const oldBit0Set = (arg & U8.BIT_0) !== 0;
        const newBit7 = (STATUS & StatusFlags.Carry) ? U8.BIT_7 : 0;
        const result = (arg >> 1) | newBit7;
        setAppropriateStatus(oldBit0Set, StatusFlags.Carry);
        setNegativeAndZero(arg);
        return arg;
    }

    function RTI() {
        STATUS = memory[popSP()];
        const low = memory[popSP()];
        const high = memory[popSP()];
        PC = (high << 8) | low;
    }

    function RTS() {
        const low = memory[popSP()];
        const high = memory[popSP()];
        const lastAddress = (high << 8) | low;
        PC = (lastAddress + 1) & U16.Max;
    }

    function setNegativeAndZero(result: number) {
        setAppropriateStatus(result === 0, StatusFlags.Zero);
        setAppropriateStatus((result & U8.BIT_7) !== 0, StatusFlags.Negative);
    }

    function PerformOnMemory(inst: (arg: number) => number, getAddr: () => number) {
        const addr = getAddr();
        const value = memory[addr];
        const result = inst(value);
        memory[addr] = result;
    }

    function bumpPC() {
        const priorValue = PC;
        PC = (PC + 1) & U16.Max;
        return priorValue;
    }

    function pushSP() {
        const priorValue = SP;
        SP = (SP - 1) & U8.Max;
        return priorValue;
    }

    function popSP() {
        const priorValue = SP;
        SP = (SP + 1) & U8.Max;
        return priorValue;
    }

    function read_u8(): number {
        return memory[bumpPC()];
    }

    function read_u16(): number {
        return read_u8() | (read_u8() << 8);
    }

    function readZeroPage() {
        return memory[readZeroPageAddress()];
    }

    function readZeroPageAddress() {
        return read_u8();
    }

    function write_u8(addr: number, value: number) {
        memory[addr] = value;
    }

    function readZeroPageX() {
        return memory[readZeroPageXAddress()];
    }

    function readZeroPageXAddress() {
        return (readZeroPageAddress() + X) & U8.Max;
    }

    function readZeroPageY() {
        return memory[readZeroPageYAddress()];
    }

    function readZeroPageYAddress() {
        return (readZeroPageAddress() + Y) & U8.Max;
    }

    function readAbsolute() {
        return memory[readAbsoluteAddress()];
    }

    function readAbsoluteAddress() {
        return read_u16();
    }

    function readAbsoluteX() {
        return memory[readAbsoluteXAddress()];
    }

    function readAbsoluteXAddress() {
        return (readAbsoluteAddress() + X) & U16.Max
    }

    function readAbsoluteY() {
        return memory[readAbsoluteYAddress()];
    }

    function readAbsoluteYAddress() {
        return (readAbsoluteAddress() + Y) & U16.Max
    }

    function readIndirectAddress() {
        const addr = readAbsoluteAddress();
        const low = memory[addr];
        const high = memory[addr + 1];
        return (high << 8) | low;
    }

    function readIndirectX() {
        return memory[readIndirectXAddress()];
    }

    function readIndirectXAddress() {
        const idx = memory[(read_u8() + X) & U8.Max];
        const low = memory[idx];
        const high = memory[idx + 1];
        return (high << 8) | low;
    }

    function readIndirectY() {
        return memory[readIndirectYAddress()];
    }

    function readIndirectYAddress() {
        const idx = memory[read_u8()];
        const low = memory[idx];
        const high = memory[idx + 1];
        return (((high << 8) | low) + Y) & U16.Max;
    }

    function setA(value: number) {
        A = value;
        setNegativeAndZero(A);
    }

    function setX(value: number) {
        var X;
        X = value;
        setNegativeAndZero(X);
    }

    function setY(value: number) {
        Y = value;
        setNegativeAndZero(Y);
    }

    function setAppropriateStatus(cond: boolean, flag: StatusFlags) {
        if (cond) {
            STATUS |= flag;
        }
        else {
            STATUS &= ~flag;
        }
    }

}
