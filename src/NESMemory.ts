
// http://wiki.nesdev.com/w/index.php/CPU_memory_map
// Note that this doesn't actually trigger the MMIO
function getEffectiveAddress(addr: number) {
    if (addr < 0x0800) {
        return addr;
    }
    // 0x0800 <= addr && ...
    if (addr <= 0x1FFF) {
        return addr % 0x0800;
    }
    if (addr < 0x2008) {
        return addr;
    }
    // 0x2008 <= addr && ...
    if (addr <= 0x3FFF) {
        return addr % 8 + 2000;
    }
    return addr;
}