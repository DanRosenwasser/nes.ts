export const enum U8 {
    BIT_0 = 1 << 0,
    BIT_1 = 1 << 1,
    BIT_2 = 1 << 2,
    BIT_3 = 1 << 3,
    BIT_4 = 1 << 4,
    BIT_5 = 1 << 5,
    BIT_6 = 1 << 6,
    BIT_7 = 1 << 7,
    Max = (1 << 8) - 1,
}

export const enum U16 {
    Min = 0,
    Max = (1 << 16) - 1,

    UpperHalf = Max & ~U8.Max,
    LowerHalf = U8.Max,
}

export const enum AddressingMode {
    Absolute,
    AbsoluteOffset,
    ZeroPage,
    ZeroPageOffset,
    Immediate,
    Relative,
    Indirect,
}

export const enum StatusFlags {
    Carry = 1 << 0,
    Zero = 1 << 1,
    InterruptDisable = 1 << 2,
    Decimal = 1 << 3,
    Break = 1 << 4,
    Unused = 1 << 5,
    Overflow = 1 << 6,
    Negative = 1 << 7,
}

export const enum Opcode {
    // Add with Carry
    ADC_Imm   = 0x69,
    ADC_ZP    = 0x65,
    ADC_ZP_X  = 0x75,
    ADC_Abs   = 0x6D,
    ADC_Abs_X = 0x7D,
    ADC_Abs_Y = 0x79,
    ADC_Ind_X = 0x61,
    ADC_Ind_Y = 0x71,

    // AND (bitwise on accumulator)
    AND_Imm = 0x29,
    AND_ZP = 0x25,
    AND_ZP_X = 0x35,
    AND_Abs = 0x2D,
    AND_Abs_X = 0x3D,
    AND_Abs_Y = 0x39,
    AND_Ind_X = 0x21,
    AND_Ind_Y = 0x31,

    ASL_Acc = 0x0A,
    ASL_ZP = 0x06,
    ASL_ZP_X = 0x16,
    ASL_ABS = 0x0E,
    ASL_ABS_X = 0x1E,

    BIT_ZP = 0x24,
    BIT_ABS = 0x2C,

    // Branch
    BPL = 0x10,
    BMI = 0x30,
    BVC = 0x50,
    BVS = 0x70,
    BCC = 0x90,
    BCS = 0xB0,
    BNE = 0xD0,
    BEQ = 0xF0,

    BRK = 0x00,

    CMP_Imm = 0xC9,
    CMP_ZP = 0xC5,
    CMP_ZP_X = 0xD5,
    CMP_Abs = 0xCD,
    CMP_Abs_X = 0xDD,
    CMP_Abs_Y = 0xD9,
    CMP_Ind_X = 0xC1,
    CMP_Ind_Y = 0xD1,

    CPX_Imm = 0xE0,
    CPX_ZP = 0xE4,
    CPX_Abs = 0xEC,

    CPY_Imm = 0xC0,
    CPY_ZP = 0xC4,
    CPY_Abs = 0xCC,

    DEC_ZP = 0xC6,
    DEC_ZP_X = 0xD6,
    DEC_Abs = 0xCE,
    DEC_Abs_X = 0xDE,

    // XOR
    EOR_Imm   = 0x49,
    EOR_ZP    = 0x45,
    EOR_ZP_X  = 0x55,
    EOR_Abs   = 0x4D,
    EOR_Abs_X = 0x5D,
    EOR_Abs_Y = 0x59,
    EOR_Ind_X = 0x41,
    EOR_Ind_Y = 0x51,

    // Flag instructions
    // CLear Carry
    CLC = 0x18,
    // SEt Carry
    SEC = 0x38,
    // CLear Interrupt
    CLI = 0x58,
    // SEt Interrupt
    SEI = 0x78,
    // CLear oVerflow
    CLV = 0xB8,
    // CLear Decimal
    CLD = 0xD8,
    // SEt Decimal
    SED = 0xF8,

    INC_ZP    = 0xE6,
    INC_ZP_X  = 0xF6,
    INC_Abs   = 0xEE,
    INC_Abs_X = 0xFE,

    JMP_Abs = 0x4C,
    JMP_Ind = 0x6C,

    JSR = 0x20,

    // LoaD Accumulator
    LDA_Imm   = 0xA9,
    LDA_ZP    = 0xA5,
    LDA_ZP_X  = 0xB5,
    LDA_Abs   = 0xAD,
    LDA_Abs_X = 0xBD,
    LDA_Abs_Y = 0xB9,
    LDA_Ind_X = 0xA1,
    LDA_Ind_Y = 0xB1,

    // LoaD X Register
    LDX_Imm   = 0xA2,
    LDX_ZP    = 0xA6,
    LDX_ZP_Y  = 0xB6,
    LDX_Abs   = 0xAE,
    LDX_Abs_Y = 0xBE,

    // LoaD Y Register
    LDY_Imm   = 0xA0,
    LDY_ZP    = 0xA4,
    LDY_ZP_X  = 0xB4,
    LDY_Abs   = 0xAC,
    LDY_Abs_X = 0xBC,

    // Logical Shift Right
    LSR_Acc   = 0x4A,
    LSR_ZP    = 0x46,
    LSR_ZP_X  = 0x56,
    LSR_Abs   = 0x4E,
    LSR_Abs_X = 0x5E,

    NOP = 0XEA,

    // OR w/ Accumulator
    ORA_Imm   = 0x09,
    ORA_ZP    = 0x05,
    ORA_ZP_X  = 0x15,
    ORA_Abs   = 0x0D,
    ORA_Abs_X = 0x1D,
    ORA_Abs_Y = 0x19,
    ORA_Ind_X = 0x01,
    ORA_Ind_Y = 0x11,

    // Register Instructions
    // A -> X
    TAX = 0xAA,
    // X -> A
    TXA = 0x8A,
    DEX = 0xCA,
    INX = 0xE8,
    TAY = 0xA8,
    TYA = 0x98,
    DEY = 0x88,
    INY = 0xC8,

    // ROtate Left
    ROL_Acc   = 0x2A,
    ROL_ZP    = 0x26,
    ROL_ZP_X  = 0x36,
    ROL_Abs   = 0x2E,
    ROL_Abs_X = 0x3E,

    // ROtate Right
    ROR_Acc   = 0x6A,
    ROR_ZP    = 0x66,
    ROR_ZP_X  = 0x76,
    ROR_Abs   = 0x6E,
    ROR_Abs_X = 0x7E,

    RTI = 0x40,

    RTS = 0x60,

    // SuBtract w/ Carry
    SBC_Imm   = 0xE9,
    SBC_ZP    = 0xE5,
    SBC_ZP_X  = 0xF5,
    SBC_Abs   = 0xED,
    SBC_Abs_X = 0xFD,
    SBC_Abs_Y = 0xF9,
    SBC_Ind_X = 0xE1,
    SBC_Ind_Y = 0xF1,

    // STore Accumulator
    STA_ZP    = 0x85,
    STA_ZP_X  = 0x95,
    STA_Abs   = 0x8D,
    STA_Abs_X = 0x9D,
    STA_Abs_Y = 0x99,
    STA_Ind_X = 0x81,
    STA_Ind_Y = 0x91,

    // Transfer X to Stack Ptr
    TXS = 0x9A,
    // Transfer Stack Ptr to X
    TSX = 0xBA,
    // Push Accumulator
    PHA = 0x48,
    // Pull Accumulator
    PLA = 0x68,
    // Push processor status
    PHP = 0x08,
    // Pull processor status
    PLP = 0x28,

    STX_ZP   = 0x86,
    STX_ZP_Y = 0x96,
    STX_Abs  = 0x8E,

    STY_ZP   = 0x84,
    STY_ZP_X = 0x94,
    STY_Abs  = 0x8C,
}