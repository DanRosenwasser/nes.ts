
function assertNever(value: never, details: string) {
    const message = `
    Unexpected value:
        ${value}
    Details:
        ${details.replace(/\r?\n/, (newline) => newline + "       ")}`
    throw new Error(message);
}
